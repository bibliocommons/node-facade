module.exports = function(router){
    var searchController = require('./controllers/search');
    router.get('/search', searchController.search);
}
