/**
 * Created by kenny on 8/28/2015.
 */
require('nodejs-model');

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var router = express.Router();              // get an instance of the express Router

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/v3/api', router);

require('./routes')(router);
router.get('/', function(req, res) {
    res.json('Welcome to the api facade!');
});

app.listen(3000, function() {
    console.log('Express is listening to http://localhost:3000');
});

var search = require.main.require('./controllers/search');



