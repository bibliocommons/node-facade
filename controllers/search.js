/**
 * Created by kenny on 8/28/2015.
 */
var SearchRequestOptions = require.main.require('./api-options').SearchRequestOptions;
var AvailabilityRequestOptions = require.main.require('./api-options').AvailabilityRequestOptions;
var BibSummaryRequestOptions = require.main.require('./api-options').BibSummaryRequestOptions;
var RatingRequestOptions = require.main.require('./api-options').RatingRequestOptions;
var SearchResponse = require.main.require('./app/models/search/search_response').SearchResponse;
var Sort = require.main.require('./app/models/search/sort');
var request = require('request');
var S = require('string');
var async = require('async');
var _ = require('underscore');

exports.search = function(req, res) {
    var queryString = {qs: req.query};
    _.extend(SearchRequestOptions, queryString);
    request(SearchRequestOptions,  function(error, response, body)
    {
        if (error)
        {
            console.error(error);
            return;
        }
        var searchResults = JSON.parse(body);
        if(response.statusCode >=200 && response.statusCode <= 300){
            async.parallel([
                    function(callback) {
                        fetchAvailability(req, searchResults.metadataIds, function (error, result) {
                            callback(error, result);
                        });
                    },
                    function(callback) {
                        fetchBibSummary(req, searchResults.metadataIds, function (error, result) {
                            callback(error, result);
                        });
                    },
                    function(callback) {
                        fetchRating(req, searchResults.metadataIds, function (error, result) {
                            callback(error, result);
                        });
                    }
                ],
                function(err, allResults){
                    if (err)
                    {
                        res.status(400).json(error);
                    }

                    var results = [];
                    searchResults.metadataIds.forEach(function(metadataId) {
                        var result = {id: metadataId};
                        allResults.forEach(function(aggregatedResult){
                            _.extend(result, aggregatedResult[metadataId]);
                        });
                        results.push(result);
                    });

                    sendResponse(res, searchResults, results);
                }
            );
        } else {
            res.status(response.statusCode).json(searchResults);
        }
    });

};

var fetchAvailability = function(req, metadataIds, availabilityResult) {
    var queryString = {qs: req.query};
    var asyncTasks = [];
    metadataIds.forEach(function(metadataId) {
        asyncTasks.push(function(callback) {
            var availabilityRequest = {};
            _.extend(availabilityRequest, AvailabilityRequestOptions);
            _.extend(availabilityRequest, queryString);
            availabilityRequest.uri = S(availabilityRequest.uri).template({metadataId: metadataId}).s;
            request(availabilityRequest, function (error, response, body) {
                if (error) {
                    callback(error);
                    return;
                }
                var availability = JSON.parse(body);
                if (response.statusCode >= 200 && response.statusCode <= 300) {
                    var metadataIdAvailability = {};
                    metadataIdAvailability['metadataId'] = metadataId;
                    metadataIdAvailability['availability'] = {availability: availability};
                    callback(null, metadataIdAvailability);
                } else {
                    callback(availability);
                }
            });
        });

    });
    async.parallel(asyncTasks, function(err, result){
        if (err)
        {
            availabilityResult(err);
        }
        var mergedResult = result.reduce(function (map, availabilityEntry){
            map[availabilityEntry.metadataId] = availabilityEntry.availability;
           return map;
        }, {});
        availabilityResult(null, mergedResult);
    })
};


var fetchBibSummary = function(req, metadataIds, bibSummaryResult) {
    var queryString = {qs: req.query};
    var asyncTasks = [];
    metadataIds.forEach(function(metadataId) {
        asyncTasks.push(function(callback) {
            var bibSummaryRequestOptions = {};
            _.extend(bibSummaryRequestOptions, BibSummaryRequestOptions);
            _.extend(bibSummaryRequestOptions, queryString);
            bibSummaryRequestOptions.uri = S(bibSummaryRequestOptions.uri).template({metadataId: metadataId}).s;
            request(bibSummaryRequestOptions, function (error, response, body) {
                if (error) {
                    callback(error);
                    return;
                }
                var bibSummary = JSON.parse(body);
                if (response.statusCode >= 200 && response.statusCode <= 300) {
                    var metadataIdBibSummary = {};
                    metadataIdBibSummary['metadataId'] = metadataId;
                    metadataIdBibSummary['bibSummary'] = bibSummary;
                    callback(null, metadataIdBibSummary);
                } else {
                    callback(bibSummary);
                }
            });
        });

    });
    async.parallel(asyncTasks, function(err, result){
        if (err)
        {
            bibSummaryResult(err);
        }
        var mergedResult = _.reduce(result, function (map, bibSummaryEntry){
            map[bibSummaryEntry.metadataId] = bibSummaryEntry.bibSummary;
            return map;
        }, {});
        bibSummaryResult(null, mergedResult);
    })
};


var fetchRating = function(req, metadataIds, ratingResult) {
    var queryString = {qs: req.query};
    var asyncTasks = [];
    metadataIds.forEach(function(metadataId) {
        asyncTasks.push(function(callback) {
            var ratingRequestOptions = {};
            _.extend(ratingRequestOptions, RatingRequestOptions);
            _.extend(ratingRequestOptions, queryString);
            ratingRequestOptions.uri = S(ratingRequestOptions.uri).template({metadataId: metadataId}).s;
            request(ratingRequestOptions, function (error, response, body) {
                if (error) {
                    callback(error);
                    return;
                }
                var rating = JSON.parse(body);
                if (response.statusCode >= 200 && response.statusCode <= 300) {
                    var metadataIdRating = {};
                    metadataIdRating['metadataId'] = metadataId;
                    metadataIdRating['rating'] = {rating: rating};
                    callback(null, metadataIdRating);
                } else {
                    callback(rating);
                }
            });
        });

    });
    async.parallel(asyncTasks, function(err, result){
        if (err)
        {
            ratingResult(err);
        }
        var mergedResult = _.reduce(result, function (map, ratingEntry){
            map[ratingEntry.metadataId] = ratingEntry.rating;
            return map;
        }, {});
        ratingResult(null, mergedResult);
    })
};


var sendResponse = function(res, solrResult, resultsWithDetail){
    var searchResponse = new SearchResponse({
        searchType: solrResult.searchType,
        query: solrResult.query,
        sortBys: solrResult.sortBys,
        pagination: solrResult.pagination,
        fields: solrResult.fields,
        results: resultsWithDetail
    });
    res.status(200).json(searchResponse);
};