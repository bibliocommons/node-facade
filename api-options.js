var Request = require('request');
var _ = require('underscore')

var RestApiBase = ({
    baseUrl: "http://localhost:8082/v3",
    preambleCRLF: true,
    postambleCRLF: true
});


var SearchRequestOptions = ({
    uri: "/search",
    method: "GET"
});

var AvailabilityRequestOptions = ({
    uri: "/search/availability/{{metadataId}}",
    method: "GET"
});

var BibSummaryRequestOptions = ({
    uri: "/bibsummary/{{metadataId}}",
    method: "GET"
});

var RatingRequestOptions = ({
    uri: "/search/rating/{{metadataId}}",
    method: "GET"
});

_.extend(SearchRequestOptions, RestApiBase);
_.extend(AvailabilityRequestOptions, RestApiBase);
_.extend(BibSummaryRequestOptions, RestApiBase);
_.extend(RatingRequestOptions, RestApiBase);
//var SearchRequest = new SearchRequestOptions();
//SearchRequest.prototype = new Request();

module.exports = {
    SearchRequestOptions: SearchRequestOptions,
    AvailabilityRequestOptions: AvailabilityRequestOptions,
    BibSummaryRequestOptions: BibSummaryRequestOptions,
    RatingRequestOptions: RatingRequestOptions
};



