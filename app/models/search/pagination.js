/**
 * Created by kenny on 8/28/2015.
 */
var t = require('tcomb');

var Pagination = t.struct({
    count: t.Number,
    limit: t.Number,
    page: t.Number,
    pages: t.Number
}, "Pagination");



module.exports={
    Pagination: Pagination
};