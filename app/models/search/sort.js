/**
 * Created by kenny on 8/28/2015.
 */
var t = require('tcomb');

var ORDER = {
    asceinding: "ascending",
    descending: "descending"
};

var SORTABLE_FIELD = t.enums({
    relevancy: "relevancy",
    newly_acquired: "newly_acquired",
    title: "title",
    author: "author",
    published_date: "published_date",
    ugc_rating: "ugc_rating"
});

var SORT_ORDER = {
    relevancy: ORDER.descending,
    newly_acquired: ORDER.descending,
    title: ORDER.asceinding,
    author: ORDER.asceinding,
    published_date: ORDER.descending,
    ugc_rating: ORDER.descending
};


var Sort = t.struct({
    id: SORTABLE_FIELD,
    applied: t.Bool
}, "Sort");


module.exports = {
    Sort: Sort,
    SORTABLE_FIELD: SORTABLE_FIELD,
    ORDER: ORDER,
    SORT_ORDER: SORT_ORDER
};