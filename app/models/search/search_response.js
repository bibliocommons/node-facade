/**
 * Created by kenny on 8/28/2015.
 */
var t = require('tcomb');
var Pagination = require('./pagination').Pagination;
var Sort = require('./sort').Sort;

var SearchResponse = t.struct({
    searchType: t.String,
    query: t.String,
    pagination: Pagination,
    sortBys: t.maybe(t.list(Sort)),
    fields: t.list(t.Obj),
    results: t.list(t.Obj)
}, "SearchResponse");


module.exports={
    SearchResponse : SearchResponse,
};